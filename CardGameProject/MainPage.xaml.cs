﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UniversalCardGame
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private CardGame _game;

        public MainPage()
        {
            this.InitializeComponent();

            //create the card game
            _game = new CardGame();
        }

        private void OnPlayCards(object sender, RoutedEventArgs e)
        {
            //go through all the cards in the deck of the game
            foreach(Card cardObj in _game.CardDeck.Cards)
            {
                //print the card in the text block control
                _txtGameBoard.Text += $"{cardObj}\n";
            }
        }

        private async void OnSwitchCards(object sender, RoutedEventArgs e)
        {
            MessageDialog dlgConfirm = new MessageDialog("Are you sure you want to switch the cards?",
                "Card Game");
            dlgConfirm.Commands.Add(new UICommand("Yes"));
            dlgConfirm.Commands.Add(new UICommand("No"));
            dlgConfirm.DefaultCommandIndex = 1; //No is default if the user presses ENTER
            dlgConfirm.CancelCommandIndex = 1; //Which answer gets selected if you ESC

            UICommand answer = await dlgConfirm.ShowAsync() as UICommand;
            if (answer.Label == "Yes")
            {
                _txtGameBoard.Text = "The cards have been switched";
            }
            else 
            {
                _txtGameBoard.Text = "The cards have been kept as is";
            }
        }
    }
}
